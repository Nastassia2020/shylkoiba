import React, {useState} from 'react';
import './App.css';
import Persons from '../Persons/Persons';
import Cockpit from '../Cockpit/Cockpit';
import AuthContext from '../context/auth-context'
import Radium, {StyleRoot} from 'radium';

function App() {
  const [personsState, setPersonsState] = useState({
      persons: [
        {id: '11', name: 'Max', age: 28},
        {id: '22', name: 'Mary', age: 29},
        {id: '33', name: 'Pit', age: 30},
      ],
  });

  const [persState, setPersState] = useState({
    showPersons: false,
  })

  const [login, setLogin] = useState({
    authenticated: false,
  })

  const nameChangedHandler = (event, id) => {
    const personIndex = personsState.persons.findIndex(p => {
      return p.id === id
    });

    const person = {...personsState.persons[personIndex]};
    person.name = event.target.value;
    const persons = [...personsState.persons];
    persons[personIndex] = person;

    setPersonsState({
      persons: persons
    })
  }

  const togglePersonsHandler = () => {
    const doesShow = persState.showPersons;
    setPersState(
      {showPersons: !doesShow})
  }

  const deletePersonHandler = (personIndex) => {
    const persons = [...personsState.persons];
    persons.splice(personIndex, 1);
    setPersonsState({persons: persons});
  }

  const loginHandler = () => {
    setLogin({authenticated: true})
  }

  let persons = null;
  if (persState.showPersons) {
    persons =
      <Persons
      persons={personsState.persons}
      clicked={deletePersonHandler}
      changed={nameChangedHandler}
      isAuthenticated={login.authenticated} />
    ;
  }

  return (
    <StyleRoot>
      <div className="App">
        <AuthContext.Provider value={{authenticated: login.authenticated, login: loginHandler,}}>
          <Cockpit showPersons={persState.showPersons}
          persons={personsState.persons}
          clicked={togglePersonsHandler}/>
          {persons}
        </AuthContext.Provider>
      </div>
    </StyleRoot>
  );
}

export default Radium(App);
