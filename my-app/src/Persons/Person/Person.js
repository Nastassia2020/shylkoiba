import './Person.css';
import Radium from 'radium';
import AuthContext from '../../context/auth-context';

const person = (props) => {
    const style = {
        '@media (min-width: 500px)': {
            width: '450px',
        }
    }

    return (
        <div className="Person" style={style}>
            <AuthContext.Consumer>
                {context => context.authenticated ? <p>Authenticated!!</p> : <p>Please log in!</p>}
            </AuthContext.Consumer>
            <p onClick={props.click}>I am {props.name} and I am {props.age} years old!</p>
            <p>{props.children}</p>
            <input type="text" onChange={props.changed} value={props.name}/>
        </div>
    )
};

export default Radium(person);