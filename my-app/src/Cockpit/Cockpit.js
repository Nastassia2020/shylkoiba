import './Cockpit.css';
import Radium, {StyleRoot} from 'radium';
import AuthContext from '../context/auth-context';

const cockpit = (props) => {
    const classes = [];
    if(props.persons.length <= 2){
      classes.push('red');
    }
    if(props.persons.length <= 1){
      classes.push('bold');
    }

    const style = {
        backgroundColor: 'green',
        color: 'white',
        font: 'inherit',
        border: '1px solid blue',
        padding: '8px',
        cursor: 'pointer',
        ':hover': {
          backgroundColor: 'lightgreen',
          color: 'black',
        }
      }

      if (props.showPersons) {
        style.backgroundColor = 'red';
        style[':hover'] = {
          backgroundColor: 'lightred',
          color: 'black',
        }
      }

    return (
        <div>
            <h1>Hi, I am Rect!</h1>
            <p className={classes.join(' ')}>This is really working!!</p>
            <button style={style} onClick={(event) => props.clicked(event)}> Switch Name </button>
            <AuthContext.Consumer>
              {context => <button onClick={context.login}>Log in</button>}
            </AuthContext.Consumer>
        </div>
    );
};

export default Radium(cockpit);